#!/usr/bin/env bash

export CPPFLAGS="-D_FORTIFY_SOURCE=0"
export CFLAGS="-Og -march=rv64gc -mabi=lp64d -mcmodel=medany -ffunction-sections -fdata-sections -fvar-tracking-assignments -fPIC -fno-plt"
export CXXFLAGS="${CFLAGS}"
export LDFLAGS="-Wl,-Map=st.map,--gc-sections"
