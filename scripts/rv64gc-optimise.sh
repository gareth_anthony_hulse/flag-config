#!/usr/bin/env bash

export CPPFLAGS="-D_FORTIFY_SOURCE=2"
export CFLAGS="-O3 -march=rv64gc -mabi=lp64d -mcmodel=medany -ftree-vectorize -floop-interchange -ftree-loop-distribution -ftree-loop-linear -floop-parallelize-all -fgraphite-identity -ftree-parallelize-loops=$(nproc) -floop-strip-mine -floop-block -fPIC -ffat-lto-objects -flto-compression-level=9 -pipe -fno-signed-zeros -fno-trapping-math -fno-plt -frename-registers -fstack-protector-all -flto=$(nproc) -pthread -DNDEBUG -fstack-clash-protection"
export CXXFLAGS="${CFLAGS} -Wp,-D_GLIBCXX_ASSERTIONS"
export LDFLAGS="-Wl,-O3,-flto,--strip-all,--sort-common,--as-needed,-z,relro,-z,now -pthread"
