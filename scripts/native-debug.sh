#!/usr/bin/env bash

export CPPFLAGS="-D_FORTIFY_SOURCE=0"
export CFLAGS="-Og -march=native -g -fvar-tracking-assignments -fPIC -fno-plt -ffunction-sections -fdata-sections"
export CXXFLAGS="${CFLAGS}"
export LDFLAGS="-Wl,-O0,--sort-common,--as-needed,-z,relro,-z,now"
